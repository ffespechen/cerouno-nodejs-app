const mongoose = require('mongoose');

const sensorSchema = new mongoose.Schema({
  nombreSensor: String,
  magnitud: String,
  valor: Number,
});

exports.Sensor = mongoose.model('Sensor', sensorSchema);
