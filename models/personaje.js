const mongoose = require('mongoose');

const personajeSchema = mongoose.Schema({
  nombre: String,
  apellido: String,
  correo: String,
  revista: String,
});

module.exports = mongoose.model('Personaje', personajeSchema);
