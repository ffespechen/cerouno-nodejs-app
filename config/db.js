const mongoose = require('mongoose');

const serverURI = 'mongodb://localhost:27017/';
const database = 'cerouno';

mongoose.connect(`${serverURI}${database}`, {
  useNewUrlParser: true,
});

mongoose.connection.once('open', () => {
  console.log('Conectado a MongoDB');
});

mongoose.connection.on('error', (error) => {
  console.log(error);
});
