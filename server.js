const mongoose = require('mongoose');
require('./config/db');

const express = require('express');
const path = require('path');
require('dotenv').config({
  path: 'variables.env',
});
const router = require('./routes');
const layouts = require('express-ejs-layouts');

// Puerto hardcodeado
const PORT = 3000;

const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// Definiciones
app.set('port', process.env.PORT || PORT);

// Uso de layouts y partials
app.use(layouts);
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, './views'));
app.use(express.static('public'));

// Cookies y sesiones

// Ruteo usando express.Router()
app.use('/', router());

// El Servidor está listo para "escuchar"
app.listen(app.get('port'), () => {
  console.log(`Servidor escuchando en puerto ${app.get('port')}`);
});
