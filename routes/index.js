const express = require('express');
const router = express.Router();
const controladorPrincipal = require('../controllers/controladorPrincipal');

// Defino el ruteo de la aplicación
module.exports = (req, res) => {
  // Rutas de la aplicación
  router.get('/', controladorPrincipal.paginaHome);
  router.get('/formulario', controladorPrincipal.mostrarFormulario);
  router.post('/formulario', controladorPrincipal.mostrarDatosFormulario);
  router.post('/lectura-sensores', controladorPrincipal.escribirDatosSensores);

  // Ejemplos de rutas para una aplicación aplicando CRUDL
  // Create
  // Read
  // Update
  // Delete
  // List

  // Listado
  router.get('/aplicacion', controladorPrincipal.listar);

  // Leer un elemento en particular
  router.get('/aplicacion/:id', controladorPrincipal.obtenerElemento);

  // Actualizar un elemento en particular
  router.put('/aplicacion/:id', controladorPrincipal.updateElemento);

  // Crear un nuevo elemento
  router.post('/aplicacion', controladorPrincipal.crearElemento);

  // Borrar un elemento particular
  router.delete('/aplicacion/:id', controladorPrincipal.borrarElemento);

  // Retornar el router
  return router;
};
