# Proyecto NodeJS de Base

Un ejemplo básico de un servidor escrito en NodeJS, usando arquitectura MVC

## Primeros pasos

Crear una carpeta con el nombre del proyecto y entrar en ella

```bash
mkdir nombre-proyecto
cd nombre-proyecto
```

## Instalaciones

Dentro de la parpeta, ejecutar

```bash
npm init
```

Completar las preguntas o [Enter] por defecto

Instalar las librerías iniciales básicas

```bash
npm i --save express
npm i --save ejs
```

O bien, en una sola línea


```bash
npm i --save express, ejs
```

**express** nos servirá para crear nuestro servidor
**ejs** es un "template engine" que nos facilitará el trabajo de renderizado de nuestras páginas con contenido dinámico

Finalmente, y como una dependencia de desarrollo instalamos **nodemon** para poder automatizar el reinicio de nuestro servidor cuando realicemos cambios en los archivos fuente

```bash
npm i --save-dev nodemon
```

## Al clonar el repositorio 

Para instalar todas las dependencias, basta con ejecutar

```bash
npm install
```


## Estructura de nuestro directorio de proyectos (MVC)

Carpeta **controller**: contiene los controladores de la aplicación. Fundamentalmente funciones que gobernarán la lógica de negocios de nuestra aplicación

Carpeta **views**: alojará los archivos que renderizará el template engine y se mostrarán al usuario

Carpeta **models**: en ella residen los archivos con las clases que definen con un enfoque orientado a objetos los datos que manejará nuestra aplicación y que se almacenarán en la DDBB

Carpeta **public**: archivos y assets estáticos que utilizará nuestra aplicación, por ejemplo: archivos de estilos CSS, imágenes, archivos de script para ejecutarse en el cliente, archivos que se enlazarán mediante links estáticos, etc.

Carpeta **routes**: en el archivo index.js defne las rutas de nuestra aplicación, para optimizar la arquitectura MVC del proyecto


## Archivos

**server.js** es el punto de entrada de nuestra aplicación

**package.json** detalla las dependencias de la aplicación como también parámetros para la ejecución, test, y otros.

**.gitignore** indica a GIT los archivos que no deben incluirse en el repositorio. Se toma un ejemplo genérico con los siguientes parámetros.

```git
node_modules
dist

# local env files
.env.local
.env.*.local

# Log files
npm-debug.log*
yarn-debug.log*
yarn-error.log*

# Editor directories and files
.vscode

```

**README.md**... este archivo
