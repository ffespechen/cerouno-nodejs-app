const Personaje = require('../models/personaje');
const { Sensor } = require('../models/sensor');

// Exporto las funciones del controlador Principal
exports.paginaHome = (req, res) => {
  // console.log('Parámetros del request');
  // console.log('Método: ', req.method);
  // console.log('Body: ', req.body);
  // console.log('Headers: ', req.headers);
  // console.log('Parámetros: ', req.params);

  res.render('home');
};

exports.mostrarFormulario = (req, res) => {
  res.render('formulario');
};

// Recibe datos post
exports.mostrarDatosFormulario = (req, res) => {
  Personaje.create(req.body, (error, savedDcoument) => {
    if (error) {
      res.send(error);
    } else {
      res.send(savedDcoument);
    }
  });
};

// Recibe datos POST y los guarda en la DDBB
exports.escribirDatosSensores = (req, res) => {
  const { nombreSensor, magnitud, valor } = req.body;
  Sensor.create(
    {
      nombreSensor,
      magnitud,
      valor,
    },
    (error, resultado) => {
      if (error) {
        res.send(error);
      } else {
        res.send(resultado);
      }
    }
  );
};

// Funciones del CRUDL
exports.listar = (req, res) => {
  res.send('LISTADO DE TODOS LOS ELEMENTOS');
};

exports.obtenerElemento = (req, res) => {
  const id = req.params.id;
  res.send(`Se LISTA el elemento que cumpla con la condición ${id}`);
};

exports.updateElemento = (req, res) => {
  const id = req.params.id;
  res.send(`Se ACTUALIZA el elemento que cumpla con la condición ${id}`);
};

exports.crearElemento = (req, res) => {
  res.send(`Se CREA un elemento nuevo con los datos recibidos del POST`);
};

exports.borrarElemento = (req, res) => {
  const id = req.params.id;
  res.send(`Se BORRA el elemento que cumpla con la condición ${id}`);
};
